import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
from apscheduler.schedulers.background import BackgroundScheduler
import time
from worker import worker

def start():
    sched = BackgroundScheduler()
    sched.add_job(worker.table, 'interval', seconds=50)
    sched.start()

    while True:
        time.sleep(1)
