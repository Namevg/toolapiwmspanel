import configparser
import os
import logging.config
import colorlog

current_file = os.path.abspath(os.path.dirname(__file__))
parent_dir = os.path.join(current_file, '../')

config = parent_dir + "configure/config.properties"

config1 = configparser.ConfigParser()
config1.read(config)
LOG_FILE1 = config1.get("general", "LOG_FILE")
LOG_FILE = parent_dir + LOG_FILE1
LOGGING_FILE_CONFIG = config1.get("general", "LOGGING_FILE_CONFIG")

logging.config.fileConfig(parent_dir +  LOGGING_FILE_CONFIG, defaults={"logfilename" : LOG_FILE})

handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter(
    '%(log_color)s%(levelname)s:%(message)s'))
logger = logging.getLogger('ver1')
logger.addHandler(handler)

