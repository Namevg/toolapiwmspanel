# -*- coding: utf-8 -*-

from django.shortcuts import render

# Create your views here.
import os
import sys
sys.path.append( os.path.join( os.path.dirname(__file__), os.path.pardir ) )
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from vhgs import main
from vhgs import es_request
from wmspanelapi.views import login_required
from vegasystem import logger

@csrf_exempt


@login_required

def vhgs(request):
	log=[]
	ipaddr=[]
	client_ip=''
	if request.method == 'POST':
		if 'submit_get' in request.POST:
			query = request.POST['query'].rstrip().lstrip()
			logger.debug("query - sdt {}".format(query))
			start = request.POST['start']

			end = request.POST['end']
			logger.debug("query-end {}".format(end))

			input_ok=True
			if query=='' or start=='' or end=='':
				input_ok=False
			#
			start=main.time_process(start)
			logger.debug("query- start {}".format(start))

			request.session['start'] = start
			end = main.time_process(end)
			logger.debug("query- end {}".format(end))

			request.session['end'] = end
			request.session['query'] = query
			request.session['input_ok'] = input_ok

			noip=None
			type_search=None
			#
			if input_ok==True:
				log=main.post_process( query,start,end )
				print (log)
				type_search = log[0]
				request.session['type_search'] = type_search
			else:
				log=(0,0)
			if log[0]=='ip' and input_ok==True:
				#return_usr='Hien tai chua the tim kiem bang ip'
				request.session['ip_input'] = query
			if log[0]=='phone' and log[1]!=[] and input_ok==True:
				for i in log[1]:
					time_write_log=i[0]
					user_agent=i[3]
					client_ip=i[1]
					if client_ip=='127.0.0.1':
						try:
							http_x_forwarded_for=i[2].split(',')
							for j in http_x_forwarded_for:
								if '127.0.0.1' not in j:
									client_ip=j+'x'
						except:
							pass
					ipaddr.append((time_write_log, client_ip, user_agent))
			else :
				noip=True

		if 'submit_ip' in request.POST:
			search_string=''
			type_search = request.session.get('type_search')
			if type_search=='phone':
				client_ip=list(set(request.POST.getlist('ip')))
				search_string = main.create_search_string(client_ip)
				#request.session['search_string'] = search_string
				print('phone')
			else:
				ip_input=request.session.get('ip_input')
				search_string=' AND %s'%ip_input
				#print search_string
				print('phone')
			#
			start=request.session.get('start')
			end = request.session.get('end')
			#print es_request.convert_time('string',start)
			query =request.session.get('query')
			input_ok=request.session.get('input_ok')
			request.session['search_string'] = search_string

			#
			error_res=main.check_case_resp_error(search_string,end,start)
			data_res=error_res[0]
			recommend_res=error_res[1]
			linkids= error_res[2]
			#

		if 'next_step' in request.POST:
			search_string=request.session.get('search_string')
			#print search_string
			start = request.session.get('start')
			end = request.session.get('end')
			query = request.session.get('query')
			input_ok = request.session.get('input_ok')
			#
			error_req=main.check_case_req_time(search_string,end,start)
			data_req=error_req[0]
			recommend_req=error_req[1]
			linkids= error_req[2]

	return render(request, 'vhgs/index.html', locals())

