from elasticsearch import Elasticsearch
import time
import os

import sys
sys.path.append( os.path.join( os.path.dirname(__file__), os.path.pardir ) )
from vegasystem import logger


def convert_time(to_type, time_):

    if to_type == 'epoch':
        # "time_write_log": "2018-08-06T18:53:03+0700",
        # "@timestamp": "2018-08-06T11:58:03.964Z",
        try:
            time_tuple = time.strptime(time_, '%Y-%m-%dT%H:%M:%S.%fZ')
            logger.debug("time_tuple {}".format(time_tuple))
            time_epoch = int(time.mktime(time_tuple)) * 1000
            return time_epoch
        except:
            pass
        try:
            time_tuple = time.strptime(time_, '%Y-%m-%dT%H:%M:%S+0700')
            time_epoch = int(time.mktime(time_tuple)) * 1000
            return time_epoch
        except:
            pass
    elif to_type == 'srting':
        return time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(time_ / 1000))
    else:
        print('Chuc nang nay chua duoc ho tro')
        pass
    return 0


def response45(query_string, lte, gte):
    print('Find response code 4xx and 5xx...\n')
    es = Elasticsearch(['http://es-api.ovp.vn:80/6340e01709a5b92871afb1908d5bc2ba/'])
    index = 'cdnlog-*'
    search_string = 'response:[400 TO 600] AND (filetype:"m3u8" OR filetype:"ts")%s' % query_string

    print(search_string)
    body = {
        "size": 50,
        "sort": [
            {
                "time_write_log": {
                    "order": "desc",
                    "unmapped_type": "boolean"
                }
            }
        ],
        "query": {
            "filtered": {
                "query": {
                    "query_string": {
                        "query": search_string,
                        "analyze_wildcard": True
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "time_write_log": {
                                        "gte": gte,
                                        "lte": lte,
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ],
                        "must_not": []
                    }
                }
            }
        }
    }
    res = es.search(index=index, body=body, request_timeout=200)
    log = []
    for i in range(len(res['hits']['hits'])):
        try:
            streaming_channel = res['hits']['hits'][i]['_source']['streaming_channel']
        except:
            streaming_channel = '-'
        host_name = res['hits']['hits'][i]['_source']['host_name']
        hostname = res['hits']['hits'][i]['_source']['hostname']
        response = res['hits']['hits'][i]['_source']['response']
        uri = res['hits']['hits'][i]['_source']['uri']
        log.append((streaming_channel, host_name, hostname, response, uri))
    return (log, search_string)


def convert_to_ip(base64, lte, gte):
    print('Find client ip from phone number...\n')
    es = Elasticsearch(['http://es-api.ovp.vn:80/6340e01709a5b92871afb1908d5bc2ba/'])
    index = 'cdnlog-*'
    query = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "query_string": {
                        "query": base64,
                        "analyze_wildcard": True
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "time_write_log": {
                                        "gte": gte,
                                        "lte": lte,
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ],
                        "must_not": []
                    }
                }
            }
        },
        "aggs": {
            "3": {
                "terms": {
                    "field": "client_ip.raw",
                    "size": 10,
                    "order": {
                        "1": "desc"
                    }
                },
                "aggs": {
                    "1": {
                        "max": {
                            "field": "time_write_log"
                        }
                    },
                    "4": {
                        "terms": {
                            "field": "http_x_forwarded_for.raw",
                            "size": 5,
                            "order": {
                                "1": "desc"
                            }
                        },
                        "aggs": {
                            "1": {
                                "max": {
                                    "field": "time_write_log"
                                }
                            },
                            "5": {
                                "terms": {
                                    "field": "user_agent.raw",
                                    "size": 5,
                                    "order": {
                                        "1": "desc"
                                    }
                                },
                                "aggs": {
                                    "1": {
                                        "max": {
                                            "field": "time_write_log"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    logger.debug("run searching")
    res = es.search(index=index, body=query, request_timeout=80)
    logger.debug("res {}".format(res))
    log = []

    for i in range(len(res['aggregations']['3']['buckets'])):
        client_ip = res['aggregations']['3']['buckets'][i]['key']
        time_write_log = res['aggregations']['3']['buckets'][i]['1']['value_as_string']
        for j in range(len(res['aggregations']['3']['buckets'][i]['4']['buckets'])):
            http_x_forwarded_for = res['aggregations']['3']['buckets'][i]['4']['buckets'][j]['key']
            for k in range(len(res['aggregations']['3']['buckets'][i]['4']['buckets'][j]['5']['buckets'])):
                user_agent = res['aggregations']['3']['buckets'][i]['4']['buckets'][j]['5']['buckets'][k]['key']
                log.append((time_write_log, client_ip, http_x_forwarded_for, user_agent))
    logger.debug("Log: {}".format(log))

    return log


def check_request_time(query, lte, gte):
    print('Check request time')
    es = Elasticsearch(['http://es-api.ovp.vn:80/6340e01709a5b92871afb1908d5bc2ba/'])
    index = 'cdnlog-*'

    search_string = '(filetype:"m3u8" OR filetype:"ts") AND request_time:[5 TO 1000]%s' % query
    body = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "query_string": {
                        "analyze_wildcard": True,
                        "query": search_string
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "time_write_log": {
                                        "gte": gte,
                                        "lte": lte,
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ],
                        "must_not": []
                    }
                }
            }
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "hitmiss.raw",
                    "size": 2,
                    "order": {
                        "_count": "desc"
                    }
                },
                "aggs": {
                    "3": {
                        "terms": {
                            "field": "host_name.raw",
                            "size": 20,
                            "order": {
                                "_count": "desc"
                            }
                        },
                        "aggs": {
                            "4": {
                                "terms": {
                                    "field": "hostname.raw",
                                    "size": 20,
                                    "order": {
                                        "_count": "desc"
                                    }
                                },
                                "aggs": {
                                    "5": {
                                        "terms": {
                                            "field": "uri.raw",
                                            "size": 20,
                                            "order": {
                                                "_count": "desc"
                                            }
                                        },
                                        "aggs": {
                                            "6": {
                                                "terms": {
                                                    "field": "client_isp.raw",
                                                    "size": 20,
                                                    "order": {
                                                        "_count": "desc"
                                                    }
                                                },
                                                "aggs": {
                                                    "7": {
                                                        "terms": {
                                                            "field": "request_time",
                                                            "size": 20,
                                                            "order": {
                                                                "_count": "desc"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    res = es.search(index=index, body=body, request_timeout=200)
    log = []
    for i in range(len(res['aggregations']['2']['buckets'])):
        hitmiss = res['aggregations']['2']['buckets'][i]['key']
        for j in range(len(res['aggregations']['2']['buckets'][i]['3']['buckets'])):
            host_name = res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['key']
            for k in range(len(res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'])):
                hostname = res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['key']
                for l in range(len(
                        res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5']['buckets'])):
                    uri = \
                    res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5']['buckets'][l][
                        'key']
                    for m in range(len(
                            res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5'][
                                'buckets'][l]['6']['buckets'])):
                        isp = \
                        res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5']['buckets'][l][
                            '6']['buckets'][m]['key']
                        for n in range(len(
                                res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5'][
                                    'buckets'][l]['6']['buckets'][m]['7']['buckets'])):
                            request_time = \
                            res['aggregations']['2']['buckets'][i]['3']['buckets'][j]['4']['buckets'][k]['5'][
                                'buckets'][l]['6']['buckets'][m]['7']['buckets'][n]['key']
                            log.append((hitmiss, request_time, host_name, hostname, isp, uri))

    # print res
    return (sorted(log, key=lambda tup: tup[1], reverse=True), search_string)


if __name__ == '__main__':
    # lte=int(time.time()) *1000
    # gte=lte-60*60*1000
    # print check_request_time('113.190.252.218',lte,gte)
    # lte=1532940330441
    #
    # gte = 1534899600000
    # lte = 1534917600000
    # print(response45('113.181.64.33', lte, gte))
    time = '2018-08-06T18:53:03+0700'
    print(convert_time('epoch', time))
#
# print convert_time('sting','2018-08-06T11:58:03.964Z')
