import base64
import sys
import time
import os
sys.path.append( os.path.join( os.path.dirname(__file__), os.path.pardir ) )

from vhgs import es_request
from vegasystem import logger

def check_phone(phone_num):
	list_head_4=['0162','0163','0164','0165','0166','0167','0168','0169','0868',
				'0123','0124','0125','0127','0129',
				'0120','0121','0122','0126','0128',
				'0188','0186',
				'0199']
	list_head_3=['096','097','098','091','094','088','090','093','089','092','099',
		     '032', '033', '034', '035', '036', '037', '038', '039', '056', '058',
		     '059','070','079','077','076','078','083','084','085','081','082', '086']
	try:
		test=int(phone_num)
	except:
		return False

	if len(phone_num)==11:
		if phone_num[:4] in list_head_4:
			return True
	elif len(phone_num)==10:
		if phone_num[:3] in list_head_3:
			return True
	else:
		pass
	return False
def check_ip(ip):
	if len(ip.split('.'))==4:
		for i in ip.split('.'):
			if 0<=int(i)<=255:
				continue
			else:
				return False
	else:
		return False
	return True

def convert_epoch(oclock,day):
	oclock=int(oclock)
	logger.debug("oclock {}".format(oclock))
	logger.debug("day {}".format(day))
	if day==None:
		time_epoch = es_request.convert_time('epoch', '%sT%d:00:00+0700' % (time.strftime("%Y-%m-%d"), oclock))

	elif day!=None:
		date= '%s' %day
		logger.debug("date {}".format(date))
		time_epoch = es_request.convert_time('epoch', '%sT%d:00:00+0700' % (time.strftime(date), oclock))
		logger.debug('time_epoch : {}'.format(time_epoch))
	else:
		return 
	return time_epoch
def check_correct_oclock(oclock):
	return 0 <= int(oclock) < 24

def check_correct_day(date):
	return 0 <= int(date.split('-')[1]) <= 12 and 1 <= int(date.split('-')[2]) <= 31

def post_process(input_, start, end):
	"""
	:param input_: data phone input
	:param start: starting time
	:param end: ending time
	:return:

	lte: ending time
	gte: starting time

	"""
	lte=int(time.time()) *1000
	gte = lte-4*60*60*1000


	if start==0 or end==0:
		raise ("Invalid time")

	else:
		gte=start
		lte=end
		logger.debug("gte {} && lte{}".format(gte, lte))

	if check_phone(input_) or check_ip(input_):
		print()
		if check_phone(input_):
			choose_ip=menu_web(str(base64.b64encode(bytes(input_.encode())), 'utf-8'),lte,gte)
			return ('phone',choose_ip)
		elif check_ip(input_):
			return ('ip',input_)
	return 
		
def time_process(time_):
	if '.' in time_:
		oclock=time_.split('.')[0]
		day=time_.split('.')[1]
		logger.debug("time process: day {}".format(day))
		if (check_correct_oclock(oclock)) and (check_correct_day(day)):
			return convert_epoch(oclock,day)
	else:
		if check_correct_oclock(time_):
			return convert_epoch(time_,None)
	return 0

def menu_web(base64, lte, gte):
	log= es_request.convert_to_ip(base64, lte, gte)
	logger.debug("menu web : log {}".format(log))
	return log

def create_search_string(client_ip):
	search_string = ' AND ('
	for i in range(len(client_ip)):
		if 'x' in client_ip[i]:
			search_string = search_string + 'http_x_forwarded_for:"%s" ' % client_ip[i][:-1]
		else:
			search_string = search_string + 'client_ip: "%s" ' % client_ip[i]
		if i >= len(client_ip) - 1:
			break
		search_string = search_string + 'OR '
	search_string = search_string + ')'
	return search_string

def check_case_resp_error(search_string,lte,gte):
	log,final_search_string= es_request.response45(search_string, lte, gte)
	resp404=[]
	resp503=[]
	data=[]
	recommend = ''
	linkids=create_linkids(final_search_string, lte, gte)

	for i in range (len(log)):
		data.append( '%d | %s | %s | %s | %s'%(log[i][3], log[i][0], log[i][1], log[i][2], log[i][4]))
		if log[i][3]==403:
			pass
		if log[i][3]==404:
			resp404.append((log[i][0], log[i][1]))

		if log[i][3]==503:
			resp503.append((log[i][1],log[i][2]))

	recommend = recommend + case404(resp404)
	recommend = recommend + case503(list(set(resp503)))

	if data==[] and recommend=='':
		return (data, 'Khong co response loi',linkids)
	return (data,recommend,linkids)

def case503(resp503):
	recommend=''
	if len(resp503)<1:
		pass
	else:
		recommend = recommend+'-----\n503 --> Kiem tra Buoc 4: %s\n'%(resp503)
	return recommend
def case404(resp404):
	channel404={}
	channel_alert=[]
	#data=''
	recommend = ''
	# ((streaming_channel,host_name,hostname, response, uri))
	for i in resp404:
		#print i
		if i[0] not in channel404:
			channel404[i[0]]=1
		else:
			if channel404[i[0]] >= 4 and (i[0] not in channel_alert):
				channel_alert.append(i[0])
				recommend = recommend+'-----Kiem tra kenh %s loi'%(i[0])
				continue
			channel404[i[0]]=channel404[i[0]]+1
	return recommend

def weblog_check(client_ip,lte,gte):
	error_mes=''
	if 1:
		error_mes = error_mes + 'No log: Log cham or IP(Phone) khong xu dung dich vu. \n'
	return error_mes

def check_case_req_time(search_string,lte,gte):
	log,final_search_string = es_request.check_request_time(search_string, lte, gte)
	data=[]
	recommend=[]
	edge_request=[]
	linkids = create_linkids(final_search_string, lte, gte)
	#(hitmiss, request_time, host_name, hostname, isp, uri)
	# recommend.append('ISP: %s'%log[0])
	for i in range(len(log)):
		data.append('%s | %d | %s | %s | %s | %s'%(log[i][0], log[i][1], log[i][2], log[i][3], log[i][4],log[i][5]))
		if log[i][0]=='HIT':
			if 'request_time >5s + hitmiss = HIT -> Mang khach hang khong on dinh. Theo doi them.' not in recommend:
				recommend.append('request_time >5s + hitmiss = HIT -> Mang khach hang khong on dinh. Theo doi them.')
		if log[i][0]=='MISS':
			if log[i][2] not in edge_request:
				edge_request.append(log[i][2])
	recommend.append('')
	if edge_request != []:
		recommend.append('\trequest_time >5s + hitmiss = MISS -> Kiem tra toc do tu origin -> server edge %s \n\t\tbang ipert '%edge_request)
		recommend.append('tren edge go lenh # iperf -s\n\t\t')
		recommend.append('tren origin go lenh # iperf -c {ip server edge} -i1 -t5 -m -P3\n')
	#print data
	#print recommend
	if data==[] and recommend==['']:
		return (data,['Khong tim thay request time cao'],linkids)
	return (data,recommend,linkids)

def create_linkids(final_search_string, lte, gte):
	from_= es_request.convert_time('srting', gte)
	to_= es_request.convert_time('srting', lte)
	url_ids_temp="http://ids.ovp.vn/app/kibana#/discover?_g=(refreshInterval: '\
				 (display:Off,pause:!f,value:0),time:(from:\'%s\',mode:absolute,to:\'%s\'))&_a=(columns:! '\
				 (client_ip,response,request_time,hitmiss,client_isp,host_name,uri),index:\'cdnlog-*\',interval:auto, '\
				 query:(query_string:(analyze_wildcard:!t,query:\'%s\')),sort:!(time_write_log,desc))" \
				 %(from_,to_,final_search_string)
	return url_ids_temp

if __name__ == '__main__':
	'''
	cdnlog cham, weblog cham-> so sanh timestamp va timewritelog 30p= 30*60*1000 ---> log cham
	cdnlog ko co, weblog co get steam----> ko co log xem stream
	cdnlog ko co, weblog ko co-----> sai so dien thoai

	client_ip none. ma van check
	ko co loi thi ko in do tieu de bang

	'''
	# try:
	# 	client_ip,lte,gte=arg_process(sys.argv)
	# 	check_case_resp_error(client_ip,lte,gte)
	# except:
	# 	pass
	# # print (arg_process(sys.argv))
	# print (convert_epoch(8,'2109-1-3'))
	# # print check_ip('123.123.124.123.1')
	# # convert_epoch()
	#
	# print(check_correct_day('2109-1-3'))
	menu_web("0868900925", "0", "12")
	print(time_process('12.2019-1-3'))
